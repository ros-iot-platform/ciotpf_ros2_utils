
from ros2topic.api import get_topic_names_and_types
from rclpy.node import Node

from typing import List, NamedTuple
import re

class AvailableTopic(NamedTuple):
    topicType: str
    topicName: str


def get_all_topics(node:Node) -> List[AvailableTopic]:
    returnValue:List[AvailableTopic] = []
    topic_names_and_types = node.get_topic_names_and_types()

    for name, t in topic_names_and_types:
        assert len(t) == 1
        returnValue.append(AvailableTopic(topicName=name, topicType=t[0]))
    return returnValue



#topicNameに合致するトピックを引っ張ってくる。
#ワイルドカードあり。
#例： /ciotpf/sensor/*/temperature
#topicType: sensor_msgs/msg/Temperature
#           std_msgs/msg/String 
#           std_msgs/msg/Bool  など
def get_topics_by_topic_name_and_type(node: Node, topicName: str, topicType: str) -> List[AvailableTopic]:
    
    #正規表現でマッチングできるようちょろっと修正を加える
    topicName = topicName.replace("*", ".*")

    topics: List[AvailableTopic] = get_all_topics(node)
    result: List[AvailableTopic] = []
    for topic in topics:
        if re.fullmatch(topicName, topic.topicName) is not None:
            if topicType == topic.topicType:
                result.append(topic)
    
    return result


def get_topics_by_topic_name(node: Node, topicName: str) -> List[AvailableTopic]:

    #正規表現でマッチングできるようちょろっと修正を加える
    topicName = topicName.replace("*", ".*")

    topics: List[AvailableTopic] = get_all_topics(node)
    result: List[AvailableTopic] = []
    for topic in topics:
        if re.fullmatch(topicName, topic.topicName) is not None:
            result.append(topic)

    return result



#t = get_topics_by_topic_name("/ciotpf/*/*/temperature")
#for _t in t:
#    print(_t.topicType)
